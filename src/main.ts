import Vue from 'vue';
import App from './App.vue';
// @ts-ignore
import PrettyCheck from 'pretty-checkbox-vue/check';

Vue.component('p-check', PrettyCheck);

Vue.config.productionTip = false;

new Vue({
    render: h => h(App)
}).$mount('#UngDung');
